//Possibilidades de respostas
let respostas = [
    "É certo", 
    "É decididamente sim", 
    "Sem sombra de dúvidas", 
    "SIM – absoluto",
    "Pode contar com isso",
    "A meu ver, sim", "Bem provável", 
    "Tem boas perspectivas", "Sim", 
    "Sinais apontam que sim", 
    "Tente novamente", "Pergunte depois",
    "Melhor não te dizer agora",
    "Não consigo prever agora",
    "Concentre-se e pergunte novamente",
    "Não conte com isso",
    "Minha resposta é não",
    "Minhas fontes dizem que não", 
    "Perspectivas não tão boas", 
    "Dúvido muito"
]

function aleatorio() {
    return Math.floor(Math.random()*20);
}

let resposta = "";
let elemento = document.createElement("span");
 
document.body.appendChild(elemento);

function rolarSorte(){
    resposta = respostas[aleatorio()]
    elemento.innerHTML = resposta;
    document.getElementById("instrucoes").remove();
}

document.addEventListener("change", rolarSorte);